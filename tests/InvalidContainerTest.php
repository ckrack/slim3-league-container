<?php

namespace ckrack\SlimLeagueContainerBridge\Tests;

use ckrack\SlimLeagueContainerBridge\App;
use League\Container\Container;
use PHPUnit\Framework\TestCase;
use InvalidArgumentException;

/**
 * Test invalid container
 */
class InvalidContainerTest extends TestCase
{
    public function testUserSettings()
    {
        $this->expectException(InvalidArgumentException::class);

        $app = new App(new \stdClass);
    }
}
