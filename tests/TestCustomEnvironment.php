<?php

namespace ckrack\SlimLeagueContainerBridge\Tests;

use Slim\Http\Environment;

/**
 * This is a Environment that differs from Slim\Http\Environment only because of the difference in the class name.
 */
class TestCustomEnvironment extends Environment
{
}
